class Person {
  constructor(protected name: string) {}

  sayHello(): void {
    console.log(`Hello, my name is ${this.name}`);
  }
}

const person1 = new Person("Stepan g dffdfsddf s f df ");
const person2 = new Person("dfsffg");
person1.sayHello();
person2.sayHello();
